"""
MODULE DS
"""

import unittest
from simplepkg.modules import skeleton


class TestSkeleton(unittest.TestCase):
    """
    CLASS DS
    """

    def setUp(self):
        """
        DS
        """
        pass

    def test_placeholder(self):
        """
        DS
        """
        self.assertEqual(0, 0)


if __name__ == '__main__':
    unittest.main()
